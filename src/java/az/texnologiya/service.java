package az.texnologiya;
import DAO.DB;
import java.io.IOException;
import java.security.GeneralSecurityException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.xml.ws.WebServiceContext;

/**
 * REST Web Service
 *
 * @author Ramil Mammadov
 */
@Path("service")
public class service {
   @Resource
   WebServiceContext wsContext; 
     
    @GET
    @Produces("application/json")
    @Consumes("text/plain")
    @Path("newsList/{page}")
    public String bankList(@Context HttpServletRequest request,@PathParam("page") int page) throws GeneralSecurityException, IOException {
        new DB().addLog(request.getRemoteAddr().toString(),"newsList-page:"+page);
        return new DB().newsList(page);
         
    }
    @GET
    @Produces("application/json")
    @Consumes("text/plain")
    @Path("reviewList/{page}")
    public String reviewList(@Context HttpServletRequest request,@PathParam("page") int page) throws GeneralSecurityException, IOException {
        new DB().addLog(request.getRemoteAddr().toString(),"reviewList-page:"+page);
        return new DB().reviewList(page);
    }
    @GET
    @Produces("application/json")
    @Consumes("text/plain")
    @Path("newsListByCategory/{categoryId}/{page}")
    public String newsListByCategory(@Context HttpServletRequest request,@PathParam("categoryId") int categoryId,@PathParam("page") int page) throws GeneralSecurityException, IOException {
        new DB().addLog(request.getRemoteAddr().toString(),"newsListByCategory-categoryId:"+categoryId+"-page:"+page);
        return new DB().newsListByCategory(categoryId,page);
    }
    @GET
    @Produces("application/json")
    @Consumes("text/plain")
    @Path("categories")
    public String categories() throws GeneralSecurityException, IOException {
           return new DB().categories();
    }
    
    
    @GET
    @Produces("application/json")
    @Consumes("text/plain")
    @Path("news/{id}/{watched}")
    public String news(@Context HttpServletRequest request,@PathParam("id") int id,@PathParam("watched") int watched) throws GeneralSecurityException, IOException {
         new DB().addLog(request.getRemoteAddr().toString(),"news-id:"+id);
        if(watched==0){
                new DB().updateMobileViews(id);
            } 
        return new DB().news(id);
           
    }
      
}
