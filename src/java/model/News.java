package model;

public class News {
    int id;
    String title;
    String description;
    String image;
    String date;
    String context;
    int views;
    String publisherName;
    String publisherSurname;

    public News(int id, String title, String description, String image, String date, String context, int views, String publisherName, String publisherSurname) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.date = date;
        this.context = context;
        this.views = views;
        this.publisherName = publisherName;
        this.publisherSurname = publisherSurname;
    }

   
   
   
}
