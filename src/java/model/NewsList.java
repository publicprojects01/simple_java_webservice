package model;

/**
 *
 * @author Ramil Mammadov
 */
public class NewsList {
    int id;
    String title;
    String description;
    String image;
    String date;
    int views;

    public NewsList(int id, String title, String description, String image, String date, int views) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.date = date;
        this.views = views;
    }
}
