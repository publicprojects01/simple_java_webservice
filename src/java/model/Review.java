package model;
 
public class Review {
    private int id;
    private String context;
    private String date_published;
    private String description;
    private String picture;
    private String publisherName;
    private String publisherSurname;
    private String title;
    private String youtube_url;

    public Review(int id, String context, String date_published, String description, String picture, String publisherName, String publisherSurname, String title, String youtube_url) {
        this.id = id;
        this.context = context;
        this.date_published = date_published;
        this.description = description;
        this.picture = picture;
        this.publisherName = publisherName;
        this.publisherSurname = publisherSurname;
        this.title = title;
        this.youtube_url = youtube_url;
    }
    
}
