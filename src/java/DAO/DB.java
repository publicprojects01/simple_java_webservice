package DAO;

import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.MobileAds;
import model.News;
import model.NewsList;
import model.Review;

public class DB {

    PreparedStatement ps;
    Connection con;
    ResultSet rs;

    public DB() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://xx.xx.xx.xx:3306/mydb?useUnicode=true&characterEncoding=utf-8", "username", "password");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void close() {
        try {
            con.close();
            ps.close();
        } catch (SQLException ex) {
            System.out.println("Error 1 :" + ex);
        }
    }

    public String newsList(int page) {
        try {
            List<NewsList> newslist = new ArrayList<>();
            String sql = "select id,subject,description,views,random_number,picture,date_published from news where status=1 order by id desc limit " + String.valueOf(10 * page)+", 10";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                newslist.add(new NewsList(rs.getInt("id"),rs.getString("subject"), rs.getString("description"), "http://texnologiya.az/imageDisplayer/image?photoName="+rs.getString("picture")+"&dir=home", rs.getString("date_published"), rs.getInt("views") + rs.getInt("random_number")));
            }
            List list=new ArrayList();
            list.add(newslist);
            list.add(ads("home"));
            return new Gson().toJson(list);
        } catch (SQLException e) {
            System.out.println("Texnologiya.az mobile webservice newsList sql exception: " + e);
            return null;
        } finally {
            close();
        }
    }
    public String reviewList(int page) {
        try {
            List<Review> newslist = new ArrayList<>();
            String sql = "select v.id,v.title,v.description,v.picture,v.date_published,v.youtube_url,u.name, u.surname from video v inner join user u on v.publisher_id=u.id  where status=1 order by id desc limit " + String.valueOf(10 * page)+", 10";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) { 
                newslist.add(new Review(rs.getInt("id"),"",
                        rs.getString("date_published"),
                        rs.getString("description"),
                        "http://texnologiya.az/imageDisplayer/review?photoName="+rs.getString("picture"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("title"),   rs.getString("youtube_url")));
            }
             List list=new ArrayList();
            list.add(newslist);
            list.add(ads("reviews"));
            return new Gson().toJson(list);
        } 
        catch (SQLException e) {
            System.out.println("Texnologiya.az mobile webservice reviewList sql exception: " + e);
            return null;
        } catch(Exception e){
               System.out.println("Texnologiya.az mobile webservice reviewList exception: " + e);
            return null;
        }finally {
            close();
        }
    }
    public String newsListByCategory(int categoryId,int page) {
        try {
            List<NewsList> newslist = new ArrayList<>();
            String sql = "select id,subject,description,views,random_number,picture,date_published from news  where status=1 and type_id=? order by id desc limit " + String.valueOf(10 * page)+", 10";
            ps = con.prepareStatement(sql);
            ps.setInt(1, categoryId);
            rs = ps.executeQuery();
            while (rs.next()) {
                newslist.add(new NewsList(rs.getInt("id"),rs.getString("subject"), rs.getString("description"), "http://texnologiya.az/imageDisplayer/image?photoName="+rs.getString("picture")+"&dir=home", rs.getString("date_published"), rs.getInt("views") + rs.getInt("random_number")));
            }
            List list=new ArrayList();
            list.add(newslist);
            list.add(ads("category"));
            return new Gson().toJson(list);
        } catch (SQLException e) {
            System.out.println("Texnologiya.az mobile webservice newsList sql exception: " + e);
            return null;
        } finally {
            close();
        }
    }
   
    public String categories() {
        try {
            List<Category> categories = new ArrayList<>();
            String sql = "select id,name from news_types";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                    categories.add(new Category(rs.getInt("id"), rs.getString("name")));
            }
            return new Gson().toJson(categories);
        } catch (SQLException e) {
            System.out.println("Texnologiya.az mobile webservice newsList sql exception: " + e);
            return null;
        } finally {
            close();
        }
    }
    public String news(int id) {
        try {
           String sql = "select n.id,n.subject,n.description,n.views,n.random_number,n.picture,n.date_published,n.context,u.name as name, u.surname as surname from news n inner join user u on n.publisher_id=u.id where n.id=? and n.status=1";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
            List list=new ArrayList();
            list.add(new News(rs.getInt("id"),rs.getString("subject"), rs.getString("description"), "http://texnologiya.az/imageDisplayer/image?photoName="+rs.getString("picture")+"&dir=home", rs.getString("date_published"), rs.getString("context"), rs.getInt("views") + rs.getInt("random_number"),rs.getString("name"),rs.getString("surname")));
            list.add(ads("news"));
            return new Gson().toJson(list); 
            }else{
                return null;
            }
        } catch (SQLException e) {
            System.out.println("Texnologiya.az mobile webservice newsList sql exception: " + e);
            return null;
        } finally {
            close();
        }
    }
    public boolean updateMobileViews(int id){
         try {
           String sql = "update news n set n.mobile_views=n.mobile_views+1 where n.id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Texnologiya.az mobile webservice updateMobileViews sql exception: " + e);
            return false;
        } finally {
            close();
        }
    }
     public boolean addLog(String ip,String page){
         try {
           String sql = "insert into mobile_log(date_created,ip,page) values(now(),?,?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, ip);
            ps.setString(2,page);
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.out.println("Texnologiya.az mobile webservice addLog sql exception: " + e);
            return false;
        } finally {
            close();
        }
    }
    
     public String ads(String location){
          try {
            List<MobileAds> ads = new ArrayList<>();
            String sql = "select image_path,url from mobile_ads where location=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, location);
            rs = ps.executeQuery();
            while (rs.next()) {
                    ads.add(new MobileAds(rs.getString("image_path"), rs.getString("url")));
            }
            return new Gson().toJson(ads);
        } catch (SQLException e) {
            System.out.println("Texnologiya.az mobile webservice newsList sql exception: " + e);
            return null;
        } finally {
            close();
        }
     }
     
}